const app = require("./config/server");

require("./app/routes/usuarios")(app);

// inicializamos el servidor
app.listen(app.get("port"), () => {
  console.log("servidor en puerto", app.get("port"));
});
