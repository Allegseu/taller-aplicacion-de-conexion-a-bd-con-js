// requerimos la conexion a la base de datos
const dbConnection = require("../../config/dbConnection");

// declaramos el modulo para encriptar la contraseña
const bcryptjs = require("bcryptjs");

module.exports = (app) => {
  const connection = dbConnection();

  app.get("/", (req, res) => {
    connection.query("select * from user", (error, results) => {
      res.render("usuarios/usuarios", {
        usuarios: results,
      });
    });
  });
  app.post("/usuarios", (req, res) => {
    const { name, email, password } = req.body;
    //  declaramos una variable para almacenar la contraseña encriptada
    var passHash = bcryptjs.hashSync(password, 8);
    connection.query(
      "insert into user SET?",
      {
        name,
        email,
        password: passHash,
      },
      (error, result) => {
        res.redirect("/");
      }
    );
  });

  //  inicion para  Poner el codigo que elimina usuarios por medio del ID

  //  fin de espacio para el codigo
};
